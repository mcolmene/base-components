
import Button from './components/Button';
import DetailsBlock from './composed/DetailsBlock';
import Dropdown from './components/Dropdown'
import Footer from './composed/Footer';
import Form from './composed/Form';
import ImageSection from './composed/ImageSection';
import ImageTextOverlay from './composed/ImageTextOverlay';
import Input from './components/Input'
import LoadingSpinner from './composed/LoadingSpinner';
import TextArea from './components/TextArea';

const components =  {
  Button,
  DetailsBlock,
  Dropdown,
  Footer,
  Form,
  Input,
  ImageSection,
  ImageTextOverlay,
  LoadingSpinner,
  TextArea
};

export default components;
