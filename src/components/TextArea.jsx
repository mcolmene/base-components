import React, { Component } from 'react';
import PropTypes from 'prop-types';
import noop from 'lodash/noop';

export default class Input extends Component {
  constructor(props) {
    super(props);
    this.onBlur = this.onBlur.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onFocus = this.onFocus.bind(this);
  }
  onBlur(event) {
    this.props.onBlur(event);
  }
  onChange(event) {
    this.props.onChange(event);
  }
  onFocus(event) {
    this.props.onFocus(event);
  }

  render() {
    const {
      className,
      htmlRef,
      id,
      maxLength,
      name,
      placeholder,
      title,
      value,
    } = this.props;

    return (
      <textarea
        className={className}
        id={id}
        maxLength={maxLength}
        name={name}
        onChange={this.onChange}
        onBlur={this.onBlur}
        onFocus={this.onFocus}
        placeholder={placeholder}
        ref={htmlRef}
        title={title}
        value={value}
        {...this.props}
      />
    );
  }
}

Input.defaultProps = {
  className: '',
  htmlRef: noop,
  id: '',
  maxLength: 150,
  name: '',
  onChange: noop,
  onBlur: noop,
  onFocus: noop,
  placeholder: '',
  title: '',
  value: '',
};

Input.propTypes = {
  className: PropTypes.string,
  htmlRef: PropTypes.func,
  id: PropTypes.string,
  maxLength: PropTypes.number,
  name: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  placeholder: PropTypes.string,
  title: PropTypes.string,
  value: PropTypes.string,
};
