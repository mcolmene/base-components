import React, { Component } from 'react';
import PropTypes from 'prop-types';
import noop from 'lodash/noop';

export default class Input extends Component {
  constructor(props) {
    super(props);
    this.onBlur = this.onBlur.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onFocus = this.onFocus.bind(this);
  }
  onBlur(event) {
    this.props.onBlur(event);
  }
  onChange(event) {
    this.props.onChange(event);
  }
  onFocus(event) {
    this.props.onFocus(event);
  }

  render() {
    const {
      className,
      htmlRef,
      id,
      name,
      title,
      type,
      value,
    } = this.props;

    return (
      <input
        className={className}
        id={id}
        name={name}
        onChange={this.onChange}
        onBlur={this.onBlur}
        onFocus={this.onFocus}
        ref={htmlRef}
        title={title}
        type={type}
        value={value}
        {...this.props}
      />
    );
  }
}

Input.defaultProps = {
  className: '',
  htmlRef: noop,
  id: '',
  name: '',
  onChange: noop,
  onBlur: noop,
  onFocus: noop,
  title: '',
  type: 'text',
  value: ''
};

Input.propTypes = {
  className: PropTypes.string,
  htmlRef: PropTypes.func,
  id: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  title: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
};
