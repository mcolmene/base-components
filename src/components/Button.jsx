import React, { Component } from 'react';
import PropTypes from 'prop-types';
import noop from 'lodash/noop';

export default class Button extends Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }
  onClick(event) {
    this.props.onClick(event);
  }

  render() {
    const {
      className,
      id,
      label,
      name,
      title = label,
      type,
      value,
    } = this.props;

    return (
      <button
        className={className}
        id={id}
        title={title}
        name={name}
        onClick={this.onClick}
        type={type}
        value={value}
        {...this.props}
      >
        {label}
      </button>
    );
  }
}

Button.defaultProps = {
  className: '',
  id: '',
  label: '',
  name: '',
  onClick: noop,
  title: '',
  type: 'button',
  value: ''
};

Button.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
    PropTypes.object
  ]),
  name: PropTypes.string,
  onClick: PropTypes.func,
  title: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
};
