'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Button = require('./components/Button');

var _Button2 = _interopRequireDefault(_Button);

var _DetailsBlock = require('./composed/DetailsBlock');

var _DetailsBlock2 = _interopRequireDefault(_DetailsBlock);

var _Dropdown = require('./components/Dropdown');

var _Dropdown2 = _interopRequireDefault(_Dropdown);

var _Footer = require('./composed/Footer');

var _Footer2 = _interopRequireDefault(_Footer);

var _Form = require('./composed/Form');

var _Form2 = _interopRequireDefault(_Form);

var _ImageSection = require('./composed/ImageSection');

var _ImageSection2 = _interopRequireDefault(_ImageSection);

var _ImageTextOverlay = require('./composed/ImageTextOverlay');

var _ImageTextOverlay2 = _interopRequireDefault(_ImageTextOverlay);

var _Input = require('./components/Input');

var _Input2 = _interopRequireDefault(_Input);

var _LoadingSpinner = require('./composed/LoadingSpinner');

var _LoadingSpinner2 = _interopRequireDefault(_LoadingSpinner);

var _TextArea = require('./components/TextArea');

var _TextArea2 = _interopRequireDefault(_TextArea);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var components = {
  Button: _Button2.default,
  DetailsBlock: _DetailsBlock2.default,
  Dropdown: _Dropdown2.default,
  Footer: _Footer2.default,
  Form: _Form2.default,
  Input: _Input2.default,
  ImageSection: _ImageSection2.default,
  ImageTextOverlay: _ImageTextOverlay2.default,
  LoadingSpinner: _LoadingSpinner2.default,
  TextArea: _TextArea2.default
};

exports.default = components;