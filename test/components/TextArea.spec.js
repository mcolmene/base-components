import React from 'react';
import { shallow } from 'enzyme';

import TextArea from '../../src/components/TextArea';


describe('TextArea test suite', () => {
  describe('Raw Component -', () => {
    const component = shallow(
      <TextArea
        type="button"
        className="class"
      >
        Click Me!
      </TextArea>
    );
    const textarea = component.find('textarea');

    it('should render a button', () => {
      expect(textarea.length).toEqual(1);
    });
  });
  describe('Component functionality -', () => {

  });
});
