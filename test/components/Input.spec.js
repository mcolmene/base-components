import React from 'react';
import { shallow } from 'enzyme';

import Input from '../../src/components/Input';

describe('Input test suite', () => {
  describe('Raw Component -', () => {
    const component = shallow(
      <Input
        className="class"
      >
        Click Me!
      </Input>
    );
    const input = component.find('input');

    it('should render a input tag', () => {
      expect(input.length).toEqual(1);
    });
    it('should render a input tag with type of \'text\' by default', () => {
      expect(input.props().type).toEqual('text');
    });
    it('should have a custom className if passed as props ', () => {
      expect(input.props().className).toEqual('class');
    });
  });
});
