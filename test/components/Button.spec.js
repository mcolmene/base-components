import React from 'react';
import { shallow, mount } from 'enzyme';

import Button from '../../src/components/Button';

describe('Button test suite', () => {
  describe('Raw Component -', () => {
    const component = shallow(
      <Button
        className="className"
        label="Click Me!"
      />
    );
    const button = component.find('button');
    it('should render a button tag', () => {
      expect(button.length).toEqual(1);
    });
    it('should render a button tag with type of \'button\' by default', () => {
      expect(button.props().type).toEqual('button');
    });
    it('should have a custom className if passed as props ', () => {
      expect(button.props().className).toEqual('className');
    });
    it('should have a button with text that reads \'Click Me!\'', () => {
      // expect(button.innerHTML).toEqual('Click Me!');
    });
  });
  describe('Component functionality -', () => {
    let component;
    const onClick = jest.fn();
    beforeEach(() => {
      component = mount(
        <Button
          onClick={onClick}
          className="className"
          label="Click Me!"
        />
      );
    });
    it('should call the function on click of the button', () => {
      component.simulate('click');
      expect(onClick.mock.calls.length).toEqual(1);
    });
  });
});
