import React from 'react';
import { shallow } from 'enzyme';

import Dropdown from '../../src/components/Dropdown';

describe('Dropdown test suite', () => {
  describe('Raw Component -', () => {
    const component = shallow(
      <Dropdown />
    );
    const select = component.find('select');
    it('should render a button', () => {
      expect(select.length).toEqual(1);
    });
  });
  describe('Component functionality -', () => {

  });
});
